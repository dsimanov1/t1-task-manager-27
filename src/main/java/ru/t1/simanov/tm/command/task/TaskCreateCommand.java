package ru.t1.simanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Create new task.";

    @NotNull
    public static final String NAME = "task-create";

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        getTaskService().create(userId, name, description);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
