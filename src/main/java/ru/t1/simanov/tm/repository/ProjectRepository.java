package ru.t1.simanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.api.repository.IProjectRepository;
import ru.t1.simanov.tm.model.Project;

import java.util.Objects;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return Objects.requireNonNull(add(project));
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return Objects.requireNonNull(add(project));
    }

}
